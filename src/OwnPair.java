/**
 * Created by aminkazemi on 1/24/2019 AD.
 */
public class OwnPair <begin , end , weight> {

    private int begin;
    private int end;
    private int weight;

    public OwnPair(int begin , int end , int weight){
        this.begin = begin;
        this.end = end;
        this.weight = weight;
    }

    public int getBegin(){return  this.begin;}
    public int getEnd(){return this.end;}
    public int getWeight(){return this.weight;}
    public String toString(){
        return this.begin + "," + this.end + "," + this.weight;
    }

}
