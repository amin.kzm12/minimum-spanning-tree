import java.util.*;

/**
 * Created by aminkazemi on 1/24/2019 AD.
 */
public class MST {

    public LinkedList<OwnPair> list;
    private int[] parent;
    private int[] storeSize;

    public MST(int size) {
        list = new LinkedList<OwnPair>();
        parent = new int[size];
        storeSize = new int[size];
        for (int i = 0; i < size; i++) {
            parent[i] = i;
            storeSize[i] = 1;
        }
    }

    public void getParent(){
        for(int i = 0 ; i<10 ; i++){
            System.out.println(this.parent[i]);
        }
    }


    public void addNode(int index, int jointNode, int weight) {
        this.list.add(new OwnPair(index, jointNode, weight));
    }


    public void printMSt() {
        for (OwnPair item : this.list) {
            System.out.println(item);
        }
    }

    public void sortMst() {
        Collections.sort(this.list, new Comparator<OwnPair>() {
            @Override
            public int compare(OwnPair o1, OwnPair o2) {
                return o1.getWeight() - o2.getWeight();
            }
        });
    }


    public OwnPair find(OwnPair p1) {
        if (this.parent[p1.getBegin()] == p1.getBegin() && this.parent[p1.getEnd()] == p1.getEnd() ){
                return p1;
        }

        OwnPair temp = new OwnPair(this.parent[p1.getBegin()], this.parent[p1.getEnd()],
                p1.getWeight());
        this.parent[p1.getBegin()] = this.find(temp).getBegin();
        this.parent[p1.getEnd()] = this.find(temp).getEnd();
        return temp;
    }


    public boolean merge(OwnPair p1) {//union set
        OwnPair result = this.find(p1);
        if (result.getBegin() == result.getEnd()) {
            return false;
        }
        if (this.storeSize[result.getBegin()] > this.storeSize[result.getEnd()]) {
            this.parent[result.getEnd()] = result.getBegin();
            this.storeSize[result.getBegin()] += this.storeSize[result.getEnd()];
        } else if (this.storeSize[result.getBegin()] <= this.storeSize[result.getEnd()]) {
            this.parent[result.getBegin()] = result.getEnd();
            this.storeSize[result.getEnd()] += this.storeSize[result.getBegin()];
        }
        return true;
    }

}
