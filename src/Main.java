import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("please enter number of vertices & edges : ");
        int vertex = input.nextInt();
        int edge = input.nextInt();
        System.out.println(vertex+"\t"+edge);
        MST mst = new MST(100);
        int counter = 0;


        int begin = 0;
        int end = 0;
        int weight = 0;
        while(counter < edge){
            begin = input.nextInt();
            end = input.nextInt();
            weight = input.nextInt();
            mst.addNode(begin , end , weight);
            counter++;
        }
        mst.sortMst();
        mst.printMSt();
//

        System.out.println("\n");
        int sum=0;
        for (OwnPair item: mst.list) {
            if (mst.merge(item)){
                System.out.println(item + "\n");
//                System.out.println(m);
                sum += item.getWeight();
            }
        }
        System.out.println(sum);
//        mst.getParent();
    }
}
